<?php

namespace Drupal\flexmail_api;

use Drupal\flexmail_api\Exception\FlexmailApiResponseException;
use Drupal\flexmail_api\Exception\FlexmailApiSoapException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\flexmail_api\Exception\FlexmailApiPluginException;

/**
 * Class FlexmailApiManager.
 *
 * This class controls all necessary information for executing a service's call
 * with corresponding parameters to FlexmailAPI's wrapper function or/and
 * FlexmailAPI's service (relies in FlexmailDrupalApi::service()).
 *
 * @package Drupal\flexmail_api
 */
class FlexmailApiManager implements FlexmailApiManagerInterface, ContainerInjectionInterface {

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The Drupal Flexmail services plugin's manager.
   *
   * @var \Drupal\flexmail_api\FlexmailPluginManager
   */
  protected $pluginManager;

  /**
   * A list with instantiated drupal flexmail service's plugins.
   *
   * @var \Drupal\flexmail_api\FlexmailPluginInterface[]
   */
  protected $pluginInstances;

  /**
   * The FlexmailAPI object.
   *
   * @var FlexmailDrupalApi
   */
  protected $flexmailDrupalApi;

  /**
   * The drupal configuration for flexmail.
   *
   * @var \Drupal\flexmail_api\Config\DrupalConfig
   */
  protected $flexmailSettings;

  /**
   * FlexmailApiManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   Give us access to flexmail settings.
   * @param \Drupal\flexmail_api\FlexmailPluginManager $flexmail_plugin_manager
   *   Give us access to flexmail services plugins.
   */
  public function __construct(ConfigManagerInterface $config_manager, FlexmailPluginManager $flexmail_plugin_manager) {

    $this->configManager = $config_manager;
    $this->pluginManager = $flexmail_plugin_manager;

    $this->flexmailSettings = $this->configManager->getConfigFactory()->get('flexmail_api.settings');

    if ($this->validFlexmailApiConfiguration()) {
      $this->flexmailDrupalApi = new FlexmailDrupalApi($this->configManager);
    }

    $this->pluginInstances = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.manager'),
      $container->get('plugin.manager.flexmail_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function serviceExecute($plugin_id, $call, array $parameters = []) {
    $response = NULL;

    try {
      $plugin = $this->instantiatePlugin($plugin_id);

      if ($plugin && $plugin->isValidCall($call)) {
        $service = $this->flexmailDrupalApi->serviceCreate($plugin);
        $response = $service->{$call}($parameters);
      }
    }
    catch (FlexmailApiPluginException $exception) {
      watchdog_exception('flexmail_api', $exception);
      $response = NULL;
    }
    catch (\SoapFault $exception) {
      watchdog_exception('flexmail_api', $exception);
      $response = NULL;
      throw new FlexmailApiSoapException($exception->getMessage());
    }
    catch (\Exception $exception) {
      $code = $exception->getCode();
      watchdog_exception('flexmail_api', $exception);
      $response = NULL;
      // Every service will have its own code errors.
      if ($code != 0 || $code === "") {
        $service_name = !empty($plugin->getServiceName()) ?
          $plugin->getServiceName() : $plugin_id;
        throw new FlexmailApiResponseException($service_name, $call, $exception->getMessage(), $exception->getCode(), $exception->getPrevious());
      }
    }

    return $response;
  }

  /**
   * Whether or not the Drupal configuration is valid.
   *
   * @return bool
   *   If configuration is valid.
   */
  public function validFlexmailApiConfiguration() {
    if (empty($this->flexmailSettings)) {
      return FALSE;
    }

    try {
      $is_valid = $this->flexmailSettings->get('wsdl') &&
        $this->flexmailSettings->get('service') &&
        $this->flexmailSettings->get('user_id') &&
        $this->flexmailSettings->get('user_token');
    }
    catch (\Exception $e) {
      $is_valid = FALSE;
      watchdog_exception('flexmail_api', $e);
    }

    return $is_valid;
  }

  /**
   * A list of instantiated services, even if they failed.
   *
   * @return array|\Drupal\flexmail_api\FlexmailPluginInterface[]
   *   Instantiated plugin services.
   */
  public function getPluginInstances() {
    return $this->pluginInstances;
  }

  /**
   * Instantiates a plugin if it is not already in the pluginInstances list.
   *
   * @param string $plugin_id
   *   Plugin id.
   * @param array $plugin_configuration
   *   Plugin configuration.
   *
   * @return \Drupal\flexmail_api\FlexmailPluginInterface
   *   An instatiated service plugin.
   *
   * @throws \Drupal\flexmail_api\Exception\FlexmailApiPluginException
   */
  protected function instantiatePlugin($plugin_id, array $plugin_configuration = []) {
    if (!array_key_exists($plugin_id, $this->getPluginInstances())) {
      try {
        // @TODO Cache?
        $this->pluginInstances[$plugin_id] = $this->pluginManager->createInstance($plugin_id, $plugin_configuration);
      }
      catch (\Exception $exception) {
        // We keep the plugin_id and we won't try to instantiate it again.
        // @TODO is it the best approach?, should we just unset it?.
        $this->pluginInstances[$plugin_id] = NULL;
        watchdog_exception('flexmail_api', $exception);
        throw new FlexmailApiPluginException($exception->getMessage());
      }
    }

    return $this->pluginInstances[$plugin_id];
  }

}
