<?php

namespace Drupal\flexmail_api\Service\Flexmail;

use Finlet\flexmail\FlexmailAPI\FlexmailAPI;
use Finlet\flexmail\FlexmailAPI\Service\FlexmailAPIServiceInterface;

/**
 * Class FlexmailApiEmailAddresses.
 *
 * @package Drupal\flexmail_api\Service
 */
class FlexmailApiEmailAddresses extends FlexmailAPI implements FlexmailAPIServiceInterface {

  /**
   * Executes GetSources.
   *
   * @param array $parameters
   *   Parameters for the call.
   *
   * @return \Finlet\flexmail\FlexmailAPI\stdClass
   *   Retrieves a list of all sources for an account.
   */
  public function getSources(array $parameters) {
    // @TODO why not $this->parseArray? FlexmailAPI is doing it this way, we
    // copy what it does.
    $request = FlexmailAPI::parseArray($parameters);

    // @TODO check __soapCall, exceptions + SoapFault.
    $response = $this->execute("GetSources", $request);
    return FlexmailAPI::stripHeader($response, $this->config->get('debug_mode'));
  }

  /**
   * Executes GetSources.
   *
   * @param array $parameters
   *   Parameters for the call.
   *
   * @return \Finlet\flexmail\FlexmailAPI\stdClass
   *   Retrieves a list of all sources for an account.
   */
  public function getEmailAddressHistory(array $parameters) {
    // @TODO why not $this->parseArray? FlexmailAPI is doing it this way, we
    // copy what it does.
    $request = FlexmailAPI::parseArray($parameters);

    // @TODO check __soapCall, exceptions + SoapFault.
    $response = $this->execute("GetEmailAddressHistory", $request);
    return FlexmailAPI::stripHeader($response, $this->config->get('debug_mode'));
  }

}
