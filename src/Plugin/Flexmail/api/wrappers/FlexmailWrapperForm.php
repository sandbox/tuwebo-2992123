<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Form service.
 *
 * @FlexmailService(
 *   id = "flexmail_form",
 *   label = @Translation("Flexmail Form"),
 *   serviceName = "Form",
 *   api = {
 *     "getAll",
 *     "getResults",
 *   }
 * )
 */
class FlexmailWrapperForm extends FlexmailPluginWrapperBase {}
