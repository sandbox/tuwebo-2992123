<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail List service.
 *
 * @FlexmailService(
 *   id = "flexmail_file",
 *   label = @Translation("Flexmail File"),
 *   serviceName = "File",
 *   api = {
 *     "put",
 *   }
 * )
 */
class FlexmailWrapperFile extends FlexmailPluginWrapperBase {}
