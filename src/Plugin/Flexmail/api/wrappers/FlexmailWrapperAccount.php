<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail List service.
 *
 * @FlexmailService(
 *   id = "flexmail_account",
 *   label = @Translation("Flexmail Account"),
 *   serviceName = "Account",
 *   api = {
 *     "getBounces",
 *     "getSubscriptions",
 *     "getUnsubscriptions",
 *     "getProfileUpdates",
 *     "getBalance",
 *   }
 * )
 */
class FlexmailWrapperAccount extends FlexmailPluginWrapperBase {}
