<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail List service.
 *
 * @FlexmailService(
 *   id = "flexmail_landing_page",
 *   label = @Translation("Flexmail Landing page"),
 *   serviceName = "LandingPage",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *   }
 * )
 */
class FlexmailServiceLandingpage extends FlexmailPluginWrapperBase {}
