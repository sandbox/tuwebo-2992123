<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Group service.
 *
 * @FlexmailService(
 *   id = "flexmail_group",
 *   label = @Translation("Flexmail Group"),
 *   serviceName = "Group",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *     "createSubscription",
 *     "deleteSubscription",
 *   }
 * )
 */
class FlexmailWrapperGroup extends FlexmailPluginWrapperBase {}
