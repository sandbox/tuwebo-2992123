<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Contact service.
 *
 * @FlexmailService(
 *   id = "flexmail_contact",
 *   label = @Translation("Flexmail Contact"),
 *   serviceName = "Contact",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "import",
 *     "history",
 *     "getAll",
 *   }
 * )
 */
class FlexmailWrapperContact extends FlexmailPluginWrapperBase {}
