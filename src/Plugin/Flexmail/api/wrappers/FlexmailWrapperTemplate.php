<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Contact service.
 *
 * @FlexmailService(
 *   id = "flexmail_template",
 *   label = @Translation("Flexmail Template"),
 *   serviceName = "Template",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *   }
 * )
 */
class FlexmailWrapperTemplate extends FlexmailPluginWrapperBase {}
