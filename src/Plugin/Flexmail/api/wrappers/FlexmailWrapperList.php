<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail List service.
 *
 * @FlexmailService(
 *   id = "flexmail_list",
 *   label = @Translation("Flexmail List"),
 *   serviceName = "List",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *   }
 * )
 */
class FlexmailWrapperList extends FlexmailPluginWrapperBase {}
