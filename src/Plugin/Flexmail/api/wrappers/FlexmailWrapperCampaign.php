<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Campaign service.
 *
 * @FlexmailService(
 *   id = "flexmail_campaign",
 *   label = @Translation("Flexmail Campaign"),
 *   serviceName = "Campaign",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *     "getSummary",
 *     "getTrackingLinks",
 *     "getTrackingLinkHits",
 *     "sendTest",
 *     "send",
 *     "history",
 *     "report",
 *   }
 * )
 */
class FlexmailWrapperCampaign extends FlexmailPluginWrapperBase {}
