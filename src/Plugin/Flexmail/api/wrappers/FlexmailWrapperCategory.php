<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Contact service.
 *
 * @FlexmailService(
 *   id = "flexmail_category",
 *   label = @Translation("Flexmail Category"),
 *   serviceName = "Category",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *   }
 * )
 */
class FlexmailWrapperCategory extends FlexmailPluginWrapperBase {}
