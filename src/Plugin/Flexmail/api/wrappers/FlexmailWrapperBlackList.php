<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail BlackList service.
 *
 * @FlexmailService(
 *   id = "flexmail_blacklist",
 *   label = @Translation("Flexmail BlackList"),
 *   serviceName = "BlackList",
 *   api = {
 *     "import",
 *   }
 * )
 */
class FlexmailWrapperBlackList extends FlexmailPluginWrapperBase {}
