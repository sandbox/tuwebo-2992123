<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\wrappers;

use Drupal\flexmail_api\FlexmailPluginWrapperBase;

/**
 * Flexmail Contact service.
 *
 * @FlexmailService(
 *   id = "flexmail_message",
 *   label = @Translation("Flexmail Message"),
 *   serviceName = "Message",
 *   api = {
 *     "create",
 *     "update",
 *     "delete",
 *     "getAll",
 *   }
 * )
 */
class FlexmailWrapperMessage extends FlexmailPluginWrapperBase {}
