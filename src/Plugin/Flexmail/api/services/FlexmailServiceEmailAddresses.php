<?php

namespace Drupal\flexmail_api\Plugin\Flexmail\api\services;

use Drupal\flexmail_api\FlexmailDrupalApiInterface;
use Drupal\flexmail_api\FlexmailPluginServiceBase;

/**
 * Flexmail Email Addresses service.
 *
 * @FlexmailService(
 *   id = "flexmail_email_addresses",
 *   label = @Translation("Flexmail Email Addresses"),
 *   serviceName = "FlexmailApiEmailAddresses",
 *   api = {
 *     "getSources",
 *     "getEmailAddressHistory",
 *   }
 * )
 */
class FlexmailServiceEmailAddresses extends FlexmailPluginServiceBase implements FlexmailDrupalApiInterface {}
