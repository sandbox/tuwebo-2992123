<?php

namespace Drupal\flexmail_api;

use Drupal\flexmail_api\Exception\FlexmailApiPluginException;

/**
 * Class FlexmailPluginWrapperBase.
 *
 * @package Drupal\flexmail_api
 */
abstract class FlexmailPluginWrapperBase extends FlexmailPluginBase implements FlexmailPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function isValidCall($call) {
    if (!in_array($call, $this->getApi())) {
      $plugin = $this->getName();
      throw new FlexmailApiPluginException("Could not find method '$call' in this plugin '$plugin'.");
    }

    return TRUE;
  }

}
