<?php
/**
 * User: agustin
 * Date: 20/08/18
 * Time: 12:43
 */

namespace Drupal\flexmail_api\Exception;

/**
 * Class FlexmailApiResponseException.
 *
 * Will help us to determine the kind of error in a FlexmailAPI response.
 *
 * @package Drupal\flexmail_api\Exception
 */
class FlexmailApiResponseException extends FlexmailApiException {


  /**
   * FlexmailAPI | DrupalFlexmailApi service throwing the error.
   *
   * @var string
   */
  protected $serviceName;

  /**
   * FlexmailAPI | DrupalFlexmailApi call (request).
   *
   * @var string
   */
  protected $call;

  /**
   * FlexmailApiResponseException constructor.
   *
   * @param string $service_name
   *   Used service for the request.
   * @param string $call
   *   Used call for the request.
   * @param string|null $message
   *   Message for the exception.
   * @param int $code
   *   Exception code.
   * @param \Exception|null $previous
   *   Previous exception.
   */
  public function __construct($service_name, $call, $message = NULL, $code = 0, \Exception $previous = NULL) {
    $this->serviceName = $service_name;
    $this->call = $call;
    parent::__construct($message, $code);
  }

  /**
   * Gets excetpion "service".
   *
   * @return string
   *   The service class name.
   */
  public function getService() {
    return $this->serviceName;
  }

  /**
   * Gets exception "call".
   *
   * @return string
   *   Call name.
   */
  public function getCall() {
    return $this->call;
  }

}
