<?php

namespace Drupal\flexmail_api\Exception;

/**
 * Class FlexmailApiPluginException.
 *
 * @package Drupal\flexmail_api\Exception
 */
class FlexmailApiPluginException extends FlexmailApiException {

}
