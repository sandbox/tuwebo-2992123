<?php

namespace Drupal\flexmail_api\Exception;

/**
 * Class FlexmailApiException.
 *
 * @package Drupal\flexmail_api\Exception
 */
class FlexmailApiException extends \Exception {

}
