<?php

namespace Drupal\flexmail_api\Config;

use Drupal\Core\Config\Config;
use Finlet\flexmail\Config\ConfigInterface;

/**
 * Class DrupalConfig.
 *
 * Needed for FlexmailAPI filnet integration.
 *
 * @package Drupal\flexmail\_api\Config
 */
class DrupalConfig implements ConfigInterface {

  private $container = array();

  /**
   * DrupalConfig constructor.
   *
   * @param \Drupal\Core\Config\Config $config
   *   A Drupal configuration object.
   */
  public function __construct(Config $config) {
    $this->set('wsdl', $config->get('wsdl'));
    $this->set('service', $config->get('service'));
    $this->set('user_id', $config->get('user_id'));
    $this->set('user_token', $config->get('user_token'));
    $this->set('debug_mode', $config->get('debug_mode'));
  }

  /**
   * Getter.
   *
   * @param string $key
   *   Key name.
   *
   * @return mixed
   *   Key value.
   */
  public function get($key) {
    return $this->set($key);
  }

  /**
   * Setter.
   *
   * @param string $key
   *   Key name.
   * @param string|null $value
   *   Key value.
   *
   * @return mixed
   *   Value if key found.
   */
  public function set($key, $value = NULL) {
    if ($value !== NULL) {
      $this->container[$key] = $value;
    }

    return $this->container[$key];
  }

}
