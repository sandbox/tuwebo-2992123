<?php

namespace Drupal\flexmail_api;

use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages Drupal plugins for Finlet\flexmail\FlexmailAPI\Service classes.
 */
class FlexmailPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/Flexmail/api';

    $plugin_definition_annotation_name = 'Drupal\flexmail_api\Annotation\FlexmailService';

    parent::__construct($subdir, $namespaces, $module_handler, 'Drupal\flexmail_api\FlexmailPluginInterface', $plugin_definition_annotation_name);

    $this->alterInfo('flexmail_api_services_info_alter');

    $this->setCacheBackend($cache_backend, 'flexmail_api_services_info');
  }

}
