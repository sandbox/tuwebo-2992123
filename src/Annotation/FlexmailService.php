<?php

namespace Drupal\flexmail_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FlexmailService annotation object.
 *
 * Contains the necessary information for defining php functions declared in
 * https://soap.flexmail.eu/3.0.0/flexmail.wsdl.
 *
 * @Annotation
 */
class FlexmailService extends Plugin {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the flexmail_api.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Wrapper or service's name.
   *
   * Finlet\flexmail\Flexmail php wrapper name or
   * Drupal\flexmail_api\Service\Flexmail class name.
   *
   * @var string
   */
  public $serviceName;

  /**
   * An array of $api calls.
   *
   * @var string[]
   */
  public $api;
}
