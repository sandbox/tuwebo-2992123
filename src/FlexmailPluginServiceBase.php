<?php

namespace Drupal\flexmail_api;

use Drupal\flexmail_api\Exception\FlexmailApiPluginException;

/**
 * Class FlexmailPluginServiceBase.
 *
 * @package Drupal\flexmail_api
 */
abstract class FlexmailPluginServiceBase extends FlexmailPluginBase implements FlexmailPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function isValidCall($call) {
    // @TODO Should check that method actualy exists in the Service class.
    $call_exists = in_array($call, $this->getApi());
    if (!$call_exists) {
      $plugin = $this->getName();
      throw new FlexmailApiPluginException("Could not find method '$call' in this plugin '$plugin'.");
    }

    return TRUE;
  }

}
