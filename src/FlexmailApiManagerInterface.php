<?php

namespace Drupal\flexmail_api;

/**
 * An interface for all FlexmailService type plugins.
 */
interface FlexmailApiManagerInterface {

  /**
   * Executes a call from a given FlexmailPluginBase plugin.
   *
   * @param string $plugin_id
   *   A valid Flexmail services plugin.
   * @param string $call
   *   An existing call as defining in the plugin.
   * @param array $parameters
   *   Any parameters needed for the call to work.
   *
   * @return FlexmailAPIInterface
   *   Same as expected from Finlet\flexmail\FlexmailAPI::execute().
   */
  public function serviceExecute($plugin_id, $call, array $parameters);

}
