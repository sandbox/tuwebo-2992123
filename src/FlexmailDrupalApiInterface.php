<?php

namespace Drupal\flexmail_api;

/**
 * Interface FlexmailDrupalApiInterface.
 *
 * Flexmail php API offers some wrappers, but these have not all functionality
 * described in the WSD/SOAP. Classes implementing this interface are intended
 * to complete or create missing ones.
 *
 * Function getServiceName should have the name of an existing class inside
 * flexmail_api/Service/Flexmail. This class should extend FlexmailAPI and
 * implement all function defined in the plugin as in
 * FlexmailPluginInterface::getApi().
 *
 * @package Drupal\flexmail_api
 */
interface FlexmailDrupalApiInterface extends FlexmailPluginInterface {}
