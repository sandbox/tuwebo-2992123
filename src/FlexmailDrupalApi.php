<?php

namespace Drupal\flexmail_api;

use Finlet\flexmail\FlexmailAPI\FlexmailAPI;
use Drupal\flexmail_api\Config\DrupalConfig;
use Drupal\Core\Config\ConfigManagerInterface;

/**
 * Class FlexmailDrupalApi.
 *
 * Extends FlexmailAPI::service() for loading proper class.
 *
 * @package Drupal\flexmail_api
 */
class FlexmailDrupalApi extends FlexmailAPI {

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigManagerInterface $config_manager) {
    $config = new DrupalConfig($config_manager->getConfigFactory()->get('flexmail_api.settings'));

    parent::__construct($config);

  }

  /**
   * Creates the FlexmailApi wrapper or the Drupal FlexmailApi Service.
   *
   * @param \Drupal\flexmail_api\FlexmailPluginInterface|string $plugin
   *   A flexmail plugin service base object.
   *
   * @return \Finlet\flexmail\FlexmailAPI\FlexmailAPI|FlexmailDrupalApiInterface
   *   A new FlexmailAPI object.
   */
  public function serviceCreate($plugin) {
    if ($plugin instanceof FlexmailDrupalApiInterface) {
      $classname = "Drupal\\flexmail_api\Service\Flexmail" . '\\' . $plugin->getServiceName();
    }
    else {
      $classname = "\Finlet\\flexmail\FlexmailAPI\Service\FlexmailAPI_{$plugin->getServiceName()}";
    }

    return new $classname($this->config);
  }

}
