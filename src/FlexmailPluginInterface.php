<?php

namespace Drupal\flexmail_api;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * An interface for all Flexmail plugins service types.
 */
interface FlexmailPluginInterface extends PluginInspectionInterface {

  /**
   * Plugin definition Name.
   *
   * @return string
   *   Returns the Name in the plugin definition.
   */
  public function getName();

  /**
   * Plugin definition getApi.
   *
   * @return string[]
   *   Return api calls.
   */
  public function getApi();

  /**
   * Plugin definition for getting serviceName.
   *
   * @return string
   *   Returns the proper serviceName.
   */
  public function getServiceName();

  /**
   * Allowed api calls for this plugin.
   *
   * @param string $call
   *   Name of the call as in getName().
   *
   * @return bool
   *   TRUE if it is a valid call.
   *
   * @throws \Drupal\flexmail_api\Exception\FlexmailApiPluginException
   */
  public function isValidCall($call);

}
