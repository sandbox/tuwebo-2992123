<?php

namespace Drupal\flexmail_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\flexmail_api\FlexmailApiManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\flexmail_api\Form
 */
class FlexmailApiSettingsForm extends ConfigFormBase {

  /**
   * Once this configuration is submitted, cached data needs to be updated.
   *
   * @var \Drupal\flexmail_api\FlexmailApiManager
   */
  protected $flexmailApiManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FlexmailApiManagerInterface $flexmail_api_manager) {
    parent::__construct($config_factory);
    $this->flexmailApiManager = $flexmail_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('flexmail_api.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flexmail_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'flexmail_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flexmail_api.settings');

    $form['global'] = [
      '#type' => 'fieldset',
      '#title' => t('Global settings FlexMail'),
    ];

    $form['global']['wsdl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('WSDL URL'),
      '#default_value' => $config->get('wsdl'),
      '#required' => TRUE,
    );

    $form['global']['service'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Service URL'),
      '#default_value' => $config->get('service'),
      '#required' => TRUE,
    );

    $form['global']['user_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#default_value' => $config->get('user_id'),
      '#required' => TRUE,
    );

    $form['global']['user_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User token'),
      '#default_value' => $config->get('user_token'),
      '#required' => TRUE,
    );

    $form['global']['debug_mode'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#return_value' => 1,
      '#default_value' => $config->get('debug_mode'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $conf = $this->config('flexmail_api.settings')
      ->set('wsdl', $values['wsdl'])
      ->set('service', $values['service'])
      ->set('user_id', $values['user_id'])
      ->set('user_token', $values['user_token'])
      ->set('debug_mode', $values['debug_mode'])
      ->save();
    $conf->save();

    parent::submitForm($form, $form_state);
  }

}
