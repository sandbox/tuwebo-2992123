<?php

namespace Drupal\flexmail_api\Form;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\flexmail_api\FlexmailApiManager;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class FlexmailApiTestForm
 *
 * @package Drupal\flexmail_api\Form
 */
class FlexmailApiTestForm extends FormBase {

  public static $flexmail;
  public static $renderer;

  /**
   * FlexmailApiTestForm constructor.
   *
   * @param \Drupal\flexmail_api\FlexmailApiManager $flexmail_manager
   *   Help us to access flexmail api.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer interface.
   */
  public function __construct(FlexmailApiManager $flexmail_manager, RendererInterface $renderer) {
    self::$flexmail = $flexmail_manager;
    self::$renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('flexmail_api.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Show content using form fields.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @NOTE: You should edit this file for debugging adding your $params if
   * necessary.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Result from flemxail.
   */
  public function show(array &$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue('plugin_id');
    $call = $form_state->getValue('call');
    // Add params here for debugging.
    $params = [];

    $ajax_response = new AjaxResponse();
    $flexmail_response = new Response();
    $flexmail_response->setContent(json_encode(self::$flexmail->serviceExecute($plugin_id, $call, $params), JSON_PRETTY_PRINT));
    $flexmail_response->headers->set('Content-Type', 'application/json');

    $ajax_response->addCommand(new HtmlCommand('#edit-output', $flexmail_response->getContent()));
    return $ajax_response;
}

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'test_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['plugin_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service to call'),
      '#description' => 'For example: flexmail_category',
      '#ajax' => [
        'callback' => '::show',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ],
    ];

    $form['call'] = [
      '#type' => 'textfield',
      '#title' => 'Flexmail Api call',
      '#description' => 'For example: getAll',
      '#suffix' => '<div class="result"><h3>Result, only displayed when elements change:</h3></div><div id="edit-output"></div>',
      '#ajax' => [
        'callback' => '::show',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ],
    ];


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue('plugin_id');
    $call = $form_state->getValue('call');

  }
}
