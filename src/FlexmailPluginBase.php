<?php

namespace Drupal\flexmail_api;

use Drupal\Component\Plugin\PluginBase;

/**
 * Class FlexmailPluginBase.
 *
 * @package Drupal\flexmail_api
 */
abstract class FlexmailPluginBase extends PluginBase implements FlexmailPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getApi() {
    return $this->pluginDefinition['api'];
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceName() {
    return $this->pluginDefinition['serviceName'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function isValidCall($call);

}
